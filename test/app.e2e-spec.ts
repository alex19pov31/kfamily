import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

  it('/v1/user/login POST (valid data)', () => {
    return request(app.getHttpServer())
      .post('/v1/user/login')
      .send({"login": "admin@admin.ru", "password": "12345678"})
      .set('Content-Type', 'application/json')
      .expect(201)
  });
  it('/v1/user/login POST (invalid data)', () => {
    return request(app.getHttpServer())
      .post('/v1/user/login')
      .send({"login": "admin@admin.ru", "password": "123456781"})
      .set('Content-Type', 'application/json')
      .expect(401)
  });
});
