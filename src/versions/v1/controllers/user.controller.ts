import { Controller, Post, Body, Headers } from '@nestjs/common';
import { UserService } from '../../../common/user/user.service';

@Controller('v1/user')
export class UserController {
  constructor(
    private readonly userService: UserService,
  ) {}

  @Post('login')
  login(@Body() body) {
    return this.userService.auth(body.login, body.password);
  }
}
