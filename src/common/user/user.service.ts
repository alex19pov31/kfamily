import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { User } from './user';
import { UserTokenService } from './token/user-token.service';

@Injectable()
export class UserService {
  private users = [
    new User('admin@admin.ru', '12345678'),
    new User('user@user.ru', '87654321'),
  ];

  constructor(
    private readonly userTokenService: UserTokenService,
  ) {}

  auth(login: string, password: string) {
    const user = this.users.find((user) => user.email === login);

    if (!user || user.password !== password) {
      throw new HttpException({
        "statusCode": 401,
        "error": "Unauthorized"
      }, HttpStatus.UNAUTHORIZED);
    }

    return {
      "token": this.userTokenService.genToken(user.email),
    }
  }
}
