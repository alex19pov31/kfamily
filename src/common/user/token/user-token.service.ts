import { Injectable } from '@nestjs/common';
import { UserToken } from './user-token';
var randomstring = require("randomstring");
@Injectable()
export class UserTokenService {
    private userToken = [];
    constructor() {
    }

    genToken(login: string):string {
        const userToken = new UserToken(randomstring.generate(33), login);
        this.userToken.push(userToken);
        return userToken.token;
    }
}
