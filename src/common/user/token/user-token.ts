export class UserToken {
  private tokenExp: Date;
  private readonly minute = 60000;
  private readonly hour = this.minute * 60;

  constructor(
    public token: string,
    public login: string,
  ) {
    this.tokenExp = new Date(Date.now() + this.hour * 3);
  }

  check(): boolean {
    return this.tokenExp.getTime() > Date.now();
  }
}
