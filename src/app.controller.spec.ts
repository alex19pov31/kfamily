import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserController } from './versions/v1/controllers/user.controller';
import { UserService } from './common/user/user.service';
import { UserTokenService } from './common/user/token/user-token.service';
import { HttpException } from '@nestjs/common';

describe('AppController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [AppController, UserController],
      providers: [AppService, UserService, UserTokenService],
    }).compile();
  });

  describe('getHello', () => {
    it('should return "Hello World!"', () => {
      const appController = app.get<AppController>(AppController);
      expect(appController.getHello()).toBe('Hello World!');
    });
  });

  describe('login', () => {
    it('should return token', () => {
      const userService = app.get<UserService>(UserService);
      const userController = app.get<UserController>(UserController);
      const authResult = userService.auth("admin@admin.ru", "12345678");
      const loginResult = userController.login({"login": "admin@admin.ru", "password": "12345678"});

      expect(authResult).toEqual({"token": authResult.token});
      expect(authResult.token.length).toEqual(33);
      expect(loginResult).toEqual({"token": loginResult.token});
      expect(loginResult.token.length).toEqual(33);
    });

    it('should return error', () => {
      var loginResult;
      const userController = app.get<UserController>(UserController);

      try {
        loginResult = userController.login({"login": "admin@admin.ru", "password": "123456781"});
      } catch(error) {
        loginResult = error.response;
      }

      expect(loginResult).toEqual({statusCode: 401, error: 'Unauthorized'});
    });
  });
});
